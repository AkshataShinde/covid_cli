module github.com/AkshataShinde/covid-cli

go 1.17

require github.com/spf13/cobra v1.2.1

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/spf13/pflag v1.0.5 // indirect
)

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef // indirect
	github.com/go-openapi/errors v0.19.8 // indirect
	github.com/go-openapi/strfmt v0.20.3 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	go.mongodb.org/mongo-driver v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
)
